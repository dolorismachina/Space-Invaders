﻿using SFML.Graphics;
using SFML.Window;

namespace SpaceInvaders
{
    /// <summary>
    /// Basic sprite based object in game that can be displayed on the screen.
    /// <para>
    /// Each GameSprite has its origin in the center by default, meaning it will always reflect half the size of the sprite.
    /// Keep this in mind when doing distance caculations, placement etc.
    /// </para>
    /// </summary>
    public class GameSprite : Sprite
    {
        public GameSprite(Vector2f position, Texture texture)
            : this(position, texture, new IntRect(0, 0, (int)(texture.Size.X), (int)(texture.Size.Y))) {}

        public GameSprite(Vector2f position, Texture texture, IntRect rectangle)
            : base(texture, rectangle)
        {
            Position = position;
            Origin = new Vector2f(TextureRect.Width * 0.5f, TextureRect.Height * 0.5f);
        }

        public virtual void Move(Vector2f distance)
        {
            Position += distance;
        }

        public virtual void Move(float x, float y)
        {
            Move(new Vector2f(x, y));
        }
    }
}
