﻿using SFML;
using SFML.Audio;
using SFML.Graphics;
using System;
using System.Collections.Generic;
using System.IO;

namespace SpaceInvaders
{
    /// <summary>
    /// Class for storing resources in a non-redundant way.
    /// </summary>
    class AssetsManager
    {
        private static string imgPath = @"assets\img";
        private static string soundPath = @"assets\sounds";

        static AssetsManager()
        {
            Sounds = new Dictionary<string, SoundBuffer>();
            Textures = new Dictionary<string, Texture>();

            RegisterTextures();
            RegisterSounds();
        }

        private static void RegisterTextures()
        {
            string[] files = Directory.GetFiles(imgPath);
            string fileName = "";

            for (int i = 0; i < files.Length; i++)
            {
                fileName = Path.GetFileNameWithoutExtension(files[i]);
                Textures.Add(fileName, new Texture(files[i]));
            }
        }

        private static void RegisterSounds()
        {
            string[] files = Directory.GetFiles(soundPath);
            string fileName = "";

            for (int i = 0; i < files.Length; i++)
            {
                fileName = Path.GetFileNameWithoutExtension(files[i]);
                Console.WriteLine(files[i]);
                Console.WriteLine(fileName);
                Sounds.Add(fileName, new SoundBuffer(files[i]));
            }
        }

        /* Possible candidates to removal due to automatic resource registering.
        public static bool AddNewTexture(string name, string location)
        {
            if (!PathRegistered(location))
            {
                try
                {
                    Textures.Add(name, new Texture(location));
                    registeredLocations.Add(location);
                    return true;
                }
                catch (LoadingFailedException)
                {
                    return false;
                }
            }

            else
                return false;

        }
        public static bool AddNewSound(string name, string location)
        {
            if (!PathRegistered(location))
            {
                try
                {
                    Sounds.Add(name, new SoundBuffer(location));
                    registeredLocations.Add(location);
                    return true;
                }
                catch (LoadingFailedException)
                {
                    return false;
                }
            }

            else
                return false; 
        }
        */

        private static bool PathRegistered(string location)
        {
            if (registeredLocations.Contains(location))
                return true;
            else
                return false;
        }

        public static Dictionary<string, SoundBuffer> Sounds { get; private set; }
        public static Dictionary<string, Texture> Textures { get; private set; }

        private static List<string> registeredLocations = new List<string>();
    }
}
