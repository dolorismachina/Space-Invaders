﻿using GCL;
using SFML.Audio;
using SFML.Window;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceInvaders
{
    public class Weapon
    {
        public float Temperature { get; private set; }
        public float MaxTemperature { get; private set; }
        public float ShotsPerSecond { get; private set; }

        Sound sound = new Sound(AssetsManager.Sounds["shootSound"]);
        DateTime timeOfLastShot = DateTime.Now;

        public Weapon()
            : this(1000, 20) { }

        public Weapon(float maxTemperature, float shotsPerSeond)
        {
            Temperature = 0;
            MaxTemperature = maxTemperature;
            ShotsPerSecond = shotsPerSeond;
        }

        public void Shoot(List<Projectile> projectiles, GameSprite owner)
        {
            if (Temperature <= MaxTemperature)
            {
                if (GCL.Utilities.GetTimeDifference(timeOfLastShot, true) > 1000 / ShotsPerSecond)
                {
                    timeOfLastShot = DateTime.Now;
                    Temperature += 5;
                    sound.Play();
                    Projectile p = new Projectile(new Vector2f(owner.Position.X, owner.Position.Y - owner.Origin.Y), AssetsManager.Textures["projectile"]);
                    projectiles.Add(p);
                }
            }
        }

        public void Cooldown()
        {
            Temperature--;
        }
    }
}
