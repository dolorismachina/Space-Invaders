﻿using SFML.Graphics;
using SFML.Window;

namespace SpaceInvaders
{
    public class Projectile : GameSprite
    {
        public Projectile(Vector2f position, Texture texture)
            : base(position, texture) {}
    }
}
