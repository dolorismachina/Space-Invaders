﻿using GCL;
using SFML.Graphics;
using SFML.Window;
using SFML.Audio;
using System;
using System.Collections.Generic;

namespace SpaceInvaders
{   
    // TODO More points = faster enemies/faster spawn rate
    class SpaceInvadersScreen : Screen
    {
        RenderTexture spriteBatchTexture;
        Sprite spriteBatch;

        AnimatedSprite anima = new AnimatedSprite(new Vector2i(74, 74), new Texture(@"assets\img\shield.png"), 24, SpriteSheetType.OneSheetPerAnimation);

        public SpaceInvadersScreen(string title, Game parent)
            : base(title, parent)
        {
            backgroundMusic.Play();
            SetupHUD();

            laneWidth = parent.Size.X / numberOfLanes;

            player = new Player(new Vector2f(Parent.Size.X * 0.5f, parent.Size.Y - 20), AssetsManager.Textures["ship"]);
            player.Scale *= 0.5f;

            playerHitSound.SoundBuffer = AssetsManager.Sounds["playerHit"];
            explosionSound.SoundBuffer = AssetsManager.Sounds["explosion"];

            spriteBatchTexture = new RenderTexture((uint)(parent.Size.X), (uint)(parent.Size.Y));
            spriteBatch = new Sprite(spriteBatchTexture.Texture);

        }

        private void SetupHUD()
        {
            weaponTemperature.Texture = AssetsManager.Textures["energyBar"];
            weaponTemperature.Position = new Vector2f(Parent.Size.X - weaponTemperature.Texture.Size.X, 0);

            healthBar.Position = new Vector2f(0, 0);
            healthBar.FillColor = Color.Red;

            scoreText.Font = new Font("assets/fonts/MOTORWERKOBLIQUE.TTF");
            scoreText.Color = Color.White;
            scoreText.Position = new Vector2f(110, 0);
            scoreText.CharacterSize = 30;
            scoreText.Style = Text.Styles.Italic;
        }

        public override void Update(double dt)
        {
            if (player.Points >= 200)
            {
                Enemy.Velocity = 200;
            }
            RemoveOutOfBoundsEnemies();
            millisecondsSinceEnemiesMoved = (int)(Utilities.GetTimeDifference(timeOfEnemiesMove, true));

            if (player.Health >= 0)
            {
                SpawnNewEnemies();
                MoveEnemies(dt);
                HandleInput(dt);
                MoveProjectiles(dt);
            }

            ProcessEnemies();

            UpdateScore();
            AnimateExplosions();

            weaponTemperature.Size = new Vector2f(player.activeWeapon.Temperature * (100/player.activeWeapon.MaxTemperature), 20);

            anima.Position = player.Position;
            anima.Animate();
        }

        int frame = 0;
        DateTime fpsTime = DateTime.Now;
        private void GetFps()
        {
            if (Utilities.GetTimeDifference(fpsTime, true) >= 1000)
            {
                fpsTime = DateTime.Now;

                frame = 0;
            }
        }

        private List<GameSprite> sprites = new List<GameSprite>();
        public override void Render()
        {
            GetFps();

            frame++;
            spriteBatchTexture.Clear();
            spriteBatchTexture.Draw(player);
            spriteBatchTexture.Draw(weaponTemperature);
            spriteBatchTexture.Draw(healthBar);
            spriteBatchTexture.Draw(scoreText);
            foreach (Projectile projecitle in projectileList)
            {
                spriteBatchTexture.Draw(projecitle);
            }

            foreach (Enemy enemy in enemies)
            {
                spriteBatchTexture.Draw(enemy);
            }

            foreach (AnimatedSprite a in explosionList)
            {
                spriteBatchTexture.Draw(a);
            }
            spriteBatchTexture.Display();

            Draw(spriteBatch);

            Draw(anima);
        }

        private void ProcessEnemies()
        {
            for (int i = enemies.Count - 1; i >= 0; i--)
            {
                // Check if enemy hits the player.
                if (Math.Abs(enemies[i].Position.Y - player.Position.Y) < 32)
                {
                    if (Math.Abs(enemies[i].Position.X - player.Position.X) < 32)
                    {
                        playerHitSound.Play();
                        enemies.Remove(enemies[i]);
                        if (!player.ShieldActive)
                        {
                            player.Health -= 10;
                            healthBar.Size = new Vector2f(player.Health, 20);
                        }
                        if (player.Points - 10 >= 0)
                        {
                            player.Points -= 10;
                        }
                    }
                }
                // Check if enemy is hit by projectile.
                CheckIfEnemyHit(enemies[i]);
            }
        }


        private Random rnd = new Random();
        private float laneWidth;
        private void SpawnNewEnemies()
        {
            if (Utilities.GetTimeDifference(timeOfLastSpawn, true) >= 100)
            {
                timeOfLastSpawn = DateTime.Now;
                for (int i = 0; i < 6; i++)
                {
                    int result = rnd.Next(0, 101);
                    if (result > 70)
                    {
                        Vector2f position = new Vector2f(i * laneWidth + laneWidth * 0.5f, -32);
                        Vector2f targetPosition = player.Position;
                        
                        Enemy enemyShip = new Enemy(position, AssetsManager.Textures["enemy"]);
                        enemyShip.Scale *= 0.5f;
                        float distx = player.Position.X - enemyShip.Position.X;
                        float disty = player.Position.Y - enemyShip.Position.Y;
                        double dist = Math.Sqrt(Math.Pow(distx, 2) + Math.Pow(disty, 2));

                        double unitX = distx / dist;
                        double unitY = disty / dist;
                        enemyShip.DirectionVector = new Vector2f((float)unitX, (float)unitY);

                        enemyShip.Rotation = (float)(Math.Atan2(-distx, disty) * (180 / Math.PI));
                        enemies.Add(enemyShip);
                    }
                }
            }
        }

        private void UpdateScore()
        {
            scoreText.DisplayedString = player.Points.ToString();
        }

        

        private void CheckIfEnemyHit(Enemy enemy)
        {
            for (int i = projectileList.Count - 1; i >= 0; i--)
            {
                if (Math.Abs(projectileList[i].Position.Y - enemy.Position.Y) < projectileList[i].Origin.Y + enemy.Origin.Y)
                {
                    if (Math.Abs(projectileList[i].Position.X - enemy.Position.X) < projectileList[i].Origin.X + enemy.Origin.X) // Hit.
                    {
                        explosionSound.Play();
                        player.Points += 10;
                        AnimatedSprite anim = new AnimatedSprite(new Vector2i(77, 79), AssetsManager.Textures["explosion"], 8, SpriteSheetType.OneRowPerAnimation);
                        anim.Position = enemy.Position;
                        explosionList.Add(anim);
                        projectileList.Remove(projectileList[i]);
                        enemies.Remove(enemy);
                    }
                }
            }
        }

        private void HandleInput(double dt)
        {
            if (isKeyPressed(Keyboard.Key.Left))
            {
                player.Move(new Vector2f((float)(-250 * dt), 0));
                if (player.Position.X - player.Origin.X < 0)
                {
                    player.Position = new Vector2f(player.Texture.Size.X * 0.5f, player.Position.Y);
                }
            }
            if (isKeyPressed(Keyboard.Key.Right))
            {
                player.Move(new Vector2f((float)(250 * dt), 0));
                if (player.Position.X + player.Origin.X > Parent.Size.X)
                {
                    player.Position = new Vector2f(Parent.Size.X - player.Texture.Size.X * 0.5f, player.Position.Y);
                }
            }
            if (isKeyPressed(Keyboard.Key.Space))
            {
                player.Shoot(projectileList);
            }
            else if (player.activeWeapon.Temperature > 0)
            {
                player.activeWeapon.Cooldown();
            }
        }

        private void AnimateExplosions()
        {
            for (int i = explosionList.Count - 1; i >= 0; i--)
            {
                if (explosionList[i].LastFrame != true)
                {
                    explosionList[i].Animate();
                }
                else
                {
                    explosionList.RemoveAt(i);
                }
            }
        }

        private void MoveProjectiles(double dt)
        {
            for (int i = projectileList.Count - 1; i >= 0; i--)
            {
                projectileList[i].Move(new Vector2f(0, -200 * (float)dt));
                if (projectileList[i].Position.Y < 0)
                {
                    projectileList.RemoveAt(i);
                }
            }
        }

        private void MoveEnemies(double dt)
        {
            foreach (Enemy enemy in enemies)
            {
                enemy.Move(enemy.DirectionVector * (Enemy.Velocity * (float)dt));
            }
        }

        private void RemoveOutOfBoundsEnemies()
        {
            for (int i = enemies.Count - 1; i >= 0; i--)
            {
                if (enemies[i].Position.Y > Parent.Size.Y)
                {
                    enemies.RemoveAt(i); 
                }
            }
        }

        private List<Enemy> enemies = new List<Enemy>();
        public List<Projectile> projectileList = new List<Projectile>();
        private int millisecondsSinceEnemiesMoved;
        private DateTime timeOfEnemiesMove = DateTime.Now;
        private DateTime timeOfLastShot = DateTime.Now;
        private Player player;

        private RectangleShape healthBar = new RectangleShape(new Vector2f(100, 20));
        private Text scoreText = new Text();

        Sound moveSound = new Sound();
        private Sound shootSound = new Sound();
        private Sound explosionSound = new Sound();
        
        private List<AnimatedSprite> explosionList = new List<AnimatedSprite>();

        private AssetsManager resources = new AssetsManager();
        private DateTime timeOfLastSpawn;

        private RectangleShape weaponTemperature = new RectangleShape(new Vector2f(0, 20));
        private Sound playerHitSound = new Sound();

        private Music backgroundMusic = new Music("assets/music/background.ogg");
        private float numberOfLanes = 6;
    }
}
