﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GCL;

namespace SpaceInvaders
{
    class Program
    {
        static void Main(string[] args)
        {
            Game game = new Game(750, 600, "Space Invaders", 60);

            SpaceInvadersScreen screen = new SpaceInvadersScreen("Space Invaders", game);
            game.AddScreen(screen);
            game.ActiveScreen = "Space Invaders";

            game.Run();
        }
    }
}
