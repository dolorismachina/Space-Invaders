﻿using GCL;
using SFML.Graphics;
using SFML.Window;
using System;
using System.Collections.Generic;

namespace Fractals
{
    class Program
    {
        public static void Main(String[] args)
        {
            RenderWindow window = new RenderWindow(new VideoMode(1000, 700), "title");
            RenderTexture tex = new RenderTexture(1000, 700);
            Sprite texSprite = new Sprite(tex.Texture);

            Fractal fractal = new Fractal();

            fractal.CreateTreeFractal(500, 700, 3, 100, 0);
            Console.WriteLine(fractal.Nodes.Count);

            while (window.IsOpen())
            {
                window.Clear();
                tex.Clear(new Color(0, 0, 0, 200
                    ));
                foreach (Shape s in fractal.Nodes)
                {
                    window.Draw(s);
                }
                tex.Display();
                window.Draw(texSprite);
                window.Display();
                Image img = window.Capture();
                img.SaveToFile("C:/i.png");
                Console.ReadLine();
            }
        }
    }
}
